const SEARCH_AROUND_BTN = document.querySelector('.search-around a');
const ERROR_MESSAGE = '位置情報が取得できませんでした';

SEARCH_AROUND_BTN.addEventListener('click', function () {
    event.preventDefault();
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(successFunc, errorFunc);
    } else {
        // 現在位置を取得できない場合の処理
        alert("あなたの端末では現在位置を取得できません。");
    }
});

// Geolocation API
function successFunc(position) {
    let latlng = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
    };
    //緯度経度から住所の取得
    getDataFromApi(URL.geo, latlng, function (res){
        if(res.status === "OK"){
            console.log(res);
            let place =res.results[0].formatted_address;
            place = place.replace('、', ' ');
            location.href = '/space/search?w=' + place;
        }
    });
}

function errorFunc(error) {
    let errorMessage = {
        0: "Unknown error",
        1: "It was not allowed",
        2: "Communication error",
        3: "Time out",
    };
    console.error(errorMessage[error.code]);
    alert(ERROR_MESSAGE);
}

//get latlng from API
function getDataFromApi(url, params, func) {
    $.ajax({
        url: url,
        dataType: 'json',
        data: params,
        cache: false,
        timeout: 10000
    })
        .done(function (res, textStatus, jqXHR) {
            console.log('%c' + textStatus, 'color:dodgerblue');
            func(res);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            //ajax error
            console.error(textStatus);
            alert(ERROR_MESSAGE)
        })
}