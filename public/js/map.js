//マップ初期表示
const MAP_ZOOM_RATE = 16;
const MAP_ELEMENT = document.getElementById('space-map');

function initMap() {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode(
        {
            'address': space.address,
            'region' : 'jp'
        },
        function (results, status) {
            if (status == 'OK') {
                //Draw Map
                let latlng = results[0].geometry.location;
                map = drawMap(latlng, MAP_ELEMENT);

                //Create Maker
                let marker = new google.maps.Marker({
                    map     : map,
                    position: latlng
                });
                console.log(latlng);
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        }
    );
}

//緯度経度でマップを表示
function drawMap(latlng, mapElem) {
    let map = new google.maps.Map(mapElem, {
        center          : latlng,
        zoom            : MAP_ZOOM_RATE,
        disableDefaultUI: true, //mapのオプションボタン群
        mapTypeId       : google.maps.MapTypeId.ROADMAP
    });
    return map;
}

//緯度経度から住所を取得
function getAddressByLatLng(latlng) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        latLng: latlng
    }, function (results, status) {
        if (status == 'OK') {
            if (results[0].geometry) {
                // 住所を取得(日本の場合だけ「日本, 」を削除)
                return results[0].formatted_address.replace(/^日本, /, '');
            }
        } else {
            console.error('GeoCoder Error.')
        }
    });
}