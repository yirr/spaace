<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('top');

Route::get('/welcome', function () {
    return view('welcome');
});

//Authルーティング一式
Auth::routes();

//Space
Route::prefix('space')->group(function () {
    //認証不要
    Route::name('space.index')
        ->get('/', 'SpaceController@index');
    Route::name('space.show')
        ->get('show/{space}/', 'SpaceController@show')
        ->where('space', '[0-9]+');
    Route::name('space.search')
        ->get('search', 'SpaceController@search');
    //要ログイン
    Route::middleware(['auth'])->group(function () {
        Route::name('space.input')
            ->get('input/{space}/', 'SpaceController@input')
            ->where('space', '[0-9]+');
        Route::name('space.reserve')
            ->get('reserve/{space}/', 'SpaceController@reserve')
            ->where('space', '[0-9]+');
        Route::name('space.purchase')
            ->post('purchase/', 'SpaceController@purchase');
        Route::name('space.check')
            ->post('check', 'SpaceController@check');
    });
});
//Resource
// index, showは上記で定義済みの為除外
Route::resource('space', 'SpaceController', ['except' => ['index', 'show']])->middleware(['auth']);

//my page
Route::prefix('home')->group(function () {
    Route::middleware(['auth'])->group(function () {
        Route::name('home.index')
            ->get('/', 'HomeController@index');
        Route::name('home.reserves')
            ->get('reserves', 'HomeController@reserves');
        Route::name('home.creates')
            ->get('creates', 'HomeController@creates');
    });
});

Route::prefix('api')->group(function () {
    Route::get('/geo', 'ApiController@geo')->name('api.geo');
});