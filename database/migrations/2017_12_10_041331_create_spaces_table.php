<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    const DEFAULT_IMG_NAME = 'no_img.jpg';
    public function up()
    {
        Schema::create('spaces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('price');
            $table->longText('description');
            $table->string('zip');
            $table->string('address');
            $table->integer('foods');
            $table->string('image')->default(self::DEFAULT_IMG_NAME);
            $table->integer('host_id');
            $table->integer('max_reserve_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spaces');
    }
}
