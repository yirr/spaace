<?php

use Faker\Generator as Faker;

$factory->define(App\Reserve::class, function (Faker $faker) {
    $users = App\User::pluck('id')->all();
    $spaces = App\Space::pluck('id')->all();

    return [
        'space_id' => $faker->randomElement($spaces),
        'user_id' => $faker->randomElement($users),
        'people' => $faker->numberBetween(1,3),
        'stay_hour' => $faker->numberBetween(2,5),
        'charge_token' => 'ch_1BXqDwGYM8Cbx4fgAig5uY3I',
        'visit_date' => $faker->dateTimeBetween('now', '+ 1 month')->format('Y-m-d H:00:00'),
    ];
});
