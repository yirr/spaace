<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SpaceOptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $options = [
            [
                'name'    => 'Wi-Fi',
                'control' => 'wifi'
            ],
            [
                'name'    => '電源',
                'control' => 'power'
            ],
            [
                'name'    => 'エアコン',
                'control' => 'air-con'
            ],
            [
                'name'    => 'トイレ',
                'control' => 'toilet'
            ],
            [
                'name'    => '喫煙OK',
                'control' => 'smoking'
            ],
        ];
        foreach ($options as $option) {
            DB::table('space_options')->insert($option + ['created_at' => Carbon::now()]);
        }
    }
}
