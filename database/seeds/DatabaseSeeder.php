<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(SpacesTableSeeder::class);
        $this->call(ReservesTableSeeder::class);
        $this->call(SpaceOptionsSeeder::class);
        $this->call(SpaceOptionRelsSeeder::class);
    }
}

/**
 * 再マイグレーションとシーディング
   art migrate:refresh --seed
 *
 */