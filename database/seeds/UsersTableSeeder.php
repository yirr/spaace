<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 5)->create();

        DB::table('users')->insert([
            'name' => 'ヒラノ ヨウスケ',
            'email' => 'a'.'@a.com',
            'password' => bcrypt('secret'),
            'remember_token' => str_random(10)
        ]);
    }
}
