<?php

use Illuminate\Database\Seeder;

class SpaceOptionRelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $spaces = App\Space::pluck('id')->all();
        $space_options = App\SpaceOption::pluck('id');

        foreach ($spaces as $space_id) {
            //関連付けるオプションの数
            $optionCnt = (int)$space_options->random();
            //関連付けるオプションの配列
            $options = $space_options->random($optionCnt);
            foreach ($options as $option){
                DB::table('space_option_rels')->insert(['space_id' => $space_id, 'space_option_id' => $option]);
            }
        }
    }
}
