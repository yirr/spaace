<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSpace;
use App\Reserve;
use App\Space;

use App\SpaceOption;
use App\SpaceOptionRel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;

class SpaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //リダイレクト
        return redirect(route('space.search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $space_options = SpaceOption::all();
        $foods = config('const.space.foods');

        return view('space.create',
            compact(
                'space_options',
                'foods'
            ));
    }

    public function check(StoreSpace $request){
        $space_options = SpaceOption::all();
        $foods = config('const.space.foods');

        $params = $request->all();
        //imageのインスタンスがセッション格納時にシリアライズできないので除外
        // 画像は[local:storage/tmp]に一時保存
        $params['image'] = basename($request->image->store('public/tmp', 'local'));

        //sessionに格納
        session()->flash('reg_space', $params);

        return view('space.check',
            compact(
                'space_options',
                'foods',
                'params'
            ));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //セッション確認
        if(!session()->has('reg_space')){
            abort('403', 'アクセスが拒否されました。');
        }
        //sessionからフォームに入力されていた値を取得
        $spaceData = session('reg_space');
        //session削除
        session()->forget('reg_space');
        //フォーム入力以外の値
        $spaceData['host_id'] = auth()->id();
        $spaceData['max_reserve_count'] = 5;

        //storage/tmpに一時保存した画像を保存
        $image = Image::make('storage/tmp/'.$spaceData['image']);
        //1600より大きかったらリサイズ
        if($image->width() > 1600){
            $image->resize(1600, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        $image->save(public_path().'/'.config('const.space.image_dir').$spaceData['image'], 100);

        //Save to DB
        $space = Space::create($spaceData);


        if(!empty($spaceData['opt'])){
            //オプションの登録
            foreach ($spaceData['opt'] as $spaceOptionId) {
                //Save to DB
                SpaceOptionRel::create(['space_id' => $space->id, 'space_option_id' => $spaceOptionId]);
            }
        }

        return view('space.store',
            compact(
                'space'
            ));
    }

    /**
     * Display the specified resource.
     *
     * @param  $space
     * @return \Illuminate\Http\Response
     */
    public function show(Space $space)
    {
        $space->address_s = $this->sliceAddress($space->address);
        $space_options = SpaceOptionRel::where('space_id', $space->id)->get();
        return view('space.show',
            compact(
                'space', 'space_options'
            ));
    }

    public function search(Request $request)
    {
        $keyword = $request->w;
        $words = preg_split('/[\s]+/', mb_convert_kana($keyword, 's'), -1, PREG_SPLIT_NO_EMPTY);

        $spaces = DB::table('spaces');
        foreach( $words as $word ){
            $spaces->orWhere('address', 'like', "%{$word}%");
        }
        $spaces = $spaces->orderBy('id', 'desc')->paginate(10);

        foreach ($spaces as $space) {
            $space->address_s =  $this->sliceAddress($space->address);
        }
        return view('space.search',
            compact(
                'spaces', 'keyword'
            ));
    }
    public function input(Space $space)
    {
        return view('space.input',
            compact(
                'space'
            ));
    }
    public function reserve(\App\Http\Requests\ReserveRequest $request, Space $space)
    {
        $params = $request->all();
        $params['sum_price'] = $space->price * (int)$params['people'] * (int)$params['stay_hour'];

        $request->session()->put('reserve', $params);

        return view('space.reserve',
            compact(
                'space',
                'params'
            ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Space $space)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Space $space)
    {
        //
    }

    public function purchase(Request $request)
    {
        //API keyのセット
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        $token = $request->stripeToken;
        $space = Space::find($request->sid);
        //sessionから取得
        $params = $request->session()->get('reserve');

        //トークンの作成
        try {
            $charge = \Stripe\Charge::create(array(
                "amount" => $params['sum_price'], // 課金額はココで調整
                "currency" => "jpy",
                "source" => $token,
                "description" => "予約料金"
            ));
        } catch (\Stripe\Error\Card $e) {
            // The card has been declined
            Session::flash ( 'fail-msg', "Error! Please Try again." );
            return Redirect::back ();
        }

        //DBに保存
        $reserve = new Reserve();
        $reserve->space_id = $space->id;
        $reserve->user_id = \Auth::user()->id;
        $reserve->stay_hour = $params['stay_hour'];
        $reserve->people = $params['people'];
        $reserve->visit_date = new Carbon($params['date'] . " " . urldecode($params['time']));
        $reserve->charge_token = $charge->id;

        $reserve->save();

        return view('space.purchase',
            compact(
                'space'
            ));
    }
    //住所を分割
    // 愛知県名古屋市中区西道12-2 -> 愛知県名古屋市中区
    private function sliceAddress($address){
        $preg_res = [];
        $pattern = '/(...??[都道府県])(.+?郡.+?[町村]|.+?市.+?区|.+?[市区町村])(.+)/u';
        preg_match($pattern, $address, $preg_res);
        return $preg_res[2];
    }
}
