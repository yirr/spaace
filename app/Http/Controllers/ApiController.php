<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    const FCM_API = [
        'KEY' => "AAAAwWv6faY:APA91bGaeyDe8WF604F2f_kLZoKv-fuhzQSLlfy23ujTwJjB7aIMsbdACOmUUZnA0xyQtzDVRbv4evgqxsvu8EY8mkeIdmiWXew2cuenKUsHXYB21Q-YLPCFUKAhM3e4X3KpSa8749mP",
        'URL' => [
            'SEND' => 'https://iid.googleapis.com/iid/v1/',
            'GET'  => 'https://fcm.googleapis.com/fcm/send'
        ]
    ];
    const GOOGLE_MAP_API = [
        'KEY' => "AIzaSyBTXPmu9i3NaU3njusVhae5reAboneG6vU",
        'URL' => 'https://maps.googleapis.com/maps/api/geocode/json',
    ];

    public function index()
    {
        return;
    }

    //Geo Reverse Coding
    // 緯度経度から住所を返す
    // https://developers.google.com/maps/documentation/geocoding/intro?hl=ja#ReverseGeocoding
    public function geo(Request $request)
    {
        $data = [
            'key' =>  self::GOOGLE_MAP_API['KEY'],
            'latlng' => "{$request->lat},{$request->lng}",
            'language' => "ja",
            'result_type' => 'locality'
        ];
        return $this->getApiCurl(self::GOOGLE_MAP_API['URL'], $data);
    }

    public function getFCM(Request $request)
    {
        $topic = 'all';
        $token = $request->token; // 前準備で作っておいた、tokenを設定
        $url = "https://iid.googleapis.com/iid/v1/{$token}/rel/topics/{$topic}";

        //配列返せばjsonで返る
        // Content-Typeもapplication/jsonになる
        return $this->postApiCurl($url, self::FCM_API['KEY'], false);
    }

    public function sendFCM($title="タイトル", $msg="メッセージ")
    {
        $url = "https://fcm.googleapis.com/fcm/send";
        $data = [
            "notification" => [
                "title" => $title,
                "body" => $msg,
                "icon" => "icon.png",
                "click_action" => "http://localhost:8000"
            ],
            "data" => [
                "param" => "param is."
            ],
            "to" => "/topics/all"
        ];

        $res = $this->postApiCurl($url, self::FCM_API['KEY'], $data);

        return $res;
    }

    public function getApiCurl($url, $data = null)
    {
        $header = [
            'Content-Type: application/json'
        ];
        if (empty($data)) {
            //411error回避の為
            $header[] = 'Content-Length: 0';
        }else{
            $url .= '?' . http_build_query($data);
        }
        logger($url);

        $option = [
            CURLOPT_RETURNTRANSFER => true, //文字列として返す
            CURLOPT_TIMEOUT => 3, // タイムアウト時間
            CURLOPT_POST => false,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false
        ];

        //dataがある場合
        if (!empty($data)) {
            $option[CURLOPT_POSTFIELDS] = json_encode($data);
        }

        $ch = curl_init($url);
        curl_setopt_array($ch, $option);

        $json = curl_exec($ch);
        $info = curl_getinfo($ch);
        $errorNo = curl_errno($ch);

        // OK以外はエラーなので空白配列を返す
        if ($errorNo !== CURLE_OK) {
            // 詳しくエラーハンドリングしたい場合はerrorNoで確認
            // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
            logger("error: " . $errorNo);
            return 0;
        }

        // 200以外のステータスコードは失敗とみなし空配列を返す
        if ($info['http_code'] !== 200) {
            logger("http_error: " . $info['http_code']);
            return 0;
        }

        // 文字列から変換
        logger(json_decode($json, true));
        return json_decode($json, true);
    }

    public function postApiCurl($url, $data = null, $authKey = null)
    {
        $header = [
            'Content-Type: application/json'
        ];
        if (empty($data)) {
            //411error回避の為
            $header[] = 'Content-Length: 0';
        }
        if(!empty($authKey)){
            $header[] = 'Authorization: key=' . $authKey;  // 前準備で取得したtokenをヘッダに含める
        }

        $option = [
            CURLOPT_RETURNTRANSFER => true, //文字列として返す
            CURLOPT_TIMEOUT => 3, // タイムアウト時間
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false
        ];

        //dataがある場合
        if (!empty($data)) {
            $option[CURLOPT_POSTFIELDS] = json_encode($data);
        }

        $ch = curl_init($url);
        curl_setopt_array($ch, $option);

        $json = curl_exec($ch);
        $info = curl_getinfo($ch);
        $errorNo = curl_errno($ch);

        // OK以外はエラーなので空白配列を返す
        if ($errorNo !== CURLE_OK) {
            // 詳しくエラーハンドリングしたい場合はerrorNoで確認
            // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
            logger("error: " . $errorNo);
            return 0;
        }

        // 200以外のステータスコードは失敗とみなし空配列を返す
        if ($info['http_code'] !== 200) {
            logger("http_error: " . $info['http_code']);
            return 0;
        }

        // 文字列から変換
        return json_decode($json, true);
    }
}