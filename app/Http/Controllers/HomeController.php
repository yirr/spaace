<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reserve;
use App\Space;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.index');
    }

    public function reserves(){
        $reserves = Reserve::where('user_id', \Auth::user()->id)
            ->orderBy('visit_date', 'DESC')
            ->get();

        return view('home.reserves',
            compact(
                'reserves'
            ));
    }
    public function creates(){
        $spaces = Space::where('host_id', auth()->id())
            ->orderBy('created_at', 'DESC')
            ->get();

        return view('home.creates',
            compact(
                'spaces'
            ));
    }
}
