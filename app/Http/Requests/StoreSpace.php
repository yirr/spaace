<?php

namespace App\Http\Requests;

use App\SpaceOption;
use Illuminate\Foundation\Http\FormRequest;

class StoreSpace extends FormRequest
{
    //画像の最小サイズ
    const IMG_MIN_H = 300;
    const IMG_MIN_W = 400;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required|integer|min:100|max:100000',
            'description' => 'required|min:3|max:500',
            'zip' => 'required',
            'address' => 'required',
            //拡張子、最小サイズ、最大容量の指定
            'image' => 'required|image|mimes:jpg,jpeg,bmp,png|dimensions:min_width='.self::IMG_MIN_W.',min_height='.self::IMG_MIN_H.'|max:3000',
            'opt' => 'exists:space_options,id',
            'foods' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'スペース名',
            'price' => '利用料金',
            'description' => '説明',
            'zip' => '郵便番号',
            'address' => '住所',
            'image' => '画像',
            'max_reserve_count' => '最大予約人数',
            'host_id' => 'ホストID',
            'opt' => 'オプション',
            'foods' => '飲食提供の有無'
        ];
    }
}
