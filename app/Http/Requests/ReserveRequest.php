<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReserveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'time'    => 'required',
            'date'    => 'required',
            'people'    => 'required',
            'stay_hour'    => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'time'=> '来訪時間',
            'date'   => '来訪日',
            'people' => '人数',
            'stay_hour' => '滞在時間',
        ];
    }
}
