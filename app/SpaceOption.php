<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpaceOption extends Model
{
    public function space_option_rels(){
        return $this->hasMany(SpaceOptionRel::class);
    }
}
