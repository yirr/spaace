<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpaceOptionRel extends Model
{
    protected $fillable = [
        'space_id',
        'space_option_id',
    ];

    protected $hidden = [
        //
    ];

    public function space()
    {
        return $this->belongsTo(Space::class);
    }

    public function space_option()
    {
        return $this->belongsTo(SpaceOption::class);
    }
}
