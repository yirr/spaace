<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Space extends Model
{
    protected $fillable = [
        'name',
        'price',
        'description',
        'zip',
        'address',
        'foods',
        'host_id',
        'image',
        'max_reserve_count',
    ];

    public function reserves(){
        return $this->hasMany(Reserve::class);
    }

    public function space_option_rels(){
        return $this->hasMany(SpaceOptionRel::class);
    }
}
