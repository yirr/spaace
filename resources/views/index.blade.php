@extends('layouts.master')

@section('title')
    トップページ
@endsection

@section('main-class')
index root
@endsection

@section('main')
    <h2>スペースを共有しよう。</h2>
    <form action="{{route('space.search')}}" method="get" name="search" class="search">
        <div class="search-box">
            <input type="text" name="w" value="" placeholder="「名古屋」などエリアを入力">
            <button href="{{route('space.search')}}" class="waves-effect">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </form>
@endsection