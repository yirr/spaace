<!-- Scripts -->
{{--jQuery--}}
<script src="/js/jquery-3.2.1.min.js"></script>
{{--Materialize--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
{{--app.js--}}
<script src="{{ mix('js/app.js') }}"></script>
{{--My JS--}}
@yield('my-js')