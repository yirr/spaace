<header>
    <nav class="lighten-1" role="navigation">
        <div class="nav-wrapper container">
            <a id="logo-container" href="{{route('top')}}" class="brand-logo">{{ Config::get('app.name') }}</a>
            <ul class="right hide-on-med-and-down">
                @include('components/main-nav')
            </ul>

            <ul id="nav-mobile" class="side-nav">
                @include('components/main-nav')
            </ul>
            <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
        </div>
    </nav>
</header>