
<li><a href="{{ route('space.search') }}">スペースを探す</a></li>
<li><a href="{{ route('space.create') }}">スペースを登録する</a></li>
@if (Route::has('login'))
    @auth
    <li><a href="{{ url('/home') }}">マイページ</a></li>
    <li>
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
            ログアウト
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>
@else
    <li><a href="{{ route('login') }}">ログイン</a></li>
    <li><a href="{{ route('register') }}">会員登録</a></li>
    @endauth
@endif