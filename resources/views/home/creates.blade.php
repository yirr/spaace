@extends('layouts.master')

@section('title')
    登録したスペース
@endsection

@section('main-class')
    home creates
@endsection

@section('main')
    <h2>マイページ</h2>
    <h3>登録したスペース</h3>
    <ul class="collection reserve-list">
        @foreach($spaces as $space)
                <li class="collection-item avatar" onclick="location.href=$(this).find('a').attr('href')">
                    <a href="{{route('space.show', ['id' => $space->id])}}">
                        <img src="{{asset(config('const.space.image_dir').$space->image)}}" alt="{{$space->name}}" class="circle">
                        <span class="title">{{$space->name}}</span>
                        <p><span class="label">登録日時</span> {{date('Y年m月d日 H:i',strtotime($space->created_at))}} <br>
                            <span class="label">予約人数</span> 1 人
                        </p>
                        @if(1) {{--予約あるとき--}}
                            <span href="#!" class="secondary-content"><i class="material-icons">check_circle</i></span>
                        @endif
                    </a>
                </li>
        @endforeach
    </ul>
@endsection