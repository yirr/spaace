@extends('layouts.master')

@section('title')
    My Page
@endsection

@section('main-class')
    home reserves
@endsection

@section('main')
    <h2>マイページ</h2>
    <ul class="collection reserve-list">
        @foreach($reserves as $reserve)
                <li class="collection-item avatar" onclick="location.href=$(this).find('a').attr('href')">
                    <a href="{{route('space.show', ['id' => $reserve->space->id])}}">
                        <img src="{{asset(config('const.space.image_dir').$reserve->space->image)}}" alt="{{$reserve->space->name}}" class="circle">
                        <span class="title">{{$reserve->space->name}}</span>
                        <p><span class="label">訪問日時</span> {{date('Y年m月d日 H:i',strtotime($reserve->visit_date))}} <br>
                            <span class="label">人数</span> {{$reserve->people}} 人
                        </p>
                        @if($reserve->visit_date < \Carbon\Carbon::now())
                            <span href="#!" class="secondary-content"><i class="material-icons">check_circle</i></span>
                        @endif
                    </a>
                </li>
        @endforeach
    </ul>
@endsection