@extends('layouts.master')

@section('title')
    My Page
@endsection

@section('main-class')
    mypage index
@endsection

@section('main')
    <h2>マイページ</h2>
    <div class="collection">
        <a href="" class="collection-item">アカウント設定</a>
        <a href="{{route('home.reserves')}}" class="collection-item">予約したスペース</a>
        <a href="{{route('home.creates')}}" class="collection-item">登録したスペース</a>
    </div>
@endsection