<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    @include('components/head')
</head>
<body class="@yield('body-class')">
<div class="wrap">
    {{--header--}}
    @include('components/header')
    {{--main--}}
    <main class="@yield('main-class')">
        @yield('main')
    </main>
    {{--footer--}}
    @yield('sections')
    @include('components/footer')
</div>
<!-- Scripts -->
@include('components/js')
</body>
</html>