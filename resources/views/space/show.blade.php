@extends('layouts.master')

@section('title')
    スペース - {{$space->name}}
@endsection

@section('main-class')
    space show
@endsection

@section('main')
    <article>
        <div class="image"
             style="background: url({{asset(config('const.space.image_dir').$space->image)}}) center center /cover no-repeat">
        </div>
        <div class="text">
            <h2 class="name">{{$space->name}}</h2>
            <div class="under-title">
                <div class="address-short">
                    <i class="material-icons">location_on</i>
                    {{$space->address_s}}
                </div>
                <div class="rate-box">
                    <div class="rate">
                        @for($i=0; $i<5; $i++)
                            <i class="fa fa-star" aria-hidden="true"></i>
                        @endfor
                        (23)
                    </div>
                </div>
            </div>
            <p class="description">
                {!! nl2br(e($space->description)) !!}
            </p>
            <div id="space-map" class="row">
                map
            </div>
            <div class="row option">
                <h4>設備</h4>
                <div class="col s10">
                    @if(!empty($space_options))
                        <ul>
                            @foreach($space_options as $option)
                                <li class="col s5">
                                    <i class="material-icons prefix">check</i>{{$option->space_option->name}}
                                </li>
                            @endforeach
                        </ul>
                    @else
                        <p>なし</p>
                    @endif
                </div>
            </div>
            <div class="row food">
                <h4>飲食の提供</h4>
                <p>{{config('const.space.foods')[$space->foods]}}</p>
            </div>
        </div>
    </article>
@endsection

@section('sections')
<seciton class="bottom-fixed-btn-box">
    <div class="price-box">
        <span class="price">¥ {{number_format($space->price)}} </span>
        <span class="par"> / 1時間</span>
    </div>
    <div class="btn-box">
        <a class="waves-effect waves-light btn" href="{{route('space.input', ['id' => $space->id])}}">
            <i class="material-icons left">check_circle</i>予約する</a>
    </div>
</seciton>
@endsection

@section('my-js')
    <script>
        let space = {
            address: "{{$space->address}}"
        };
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTXPmu9i3NaU3njusVhae5reAboneG6vU&callback=initMap" async defer></script>
    <script src="{{asset('js/map.js')}}"></script>
@endsection