{{-- Require SpaceData($space) --}}
<form class="reserve" name="reserve" method="get" action="{{route('space.reserve', ['id' => $space->id])}}" id="reserve">
    @if (Session::has('fail-msg'))
        <div class="alert alert-error">{{ Session::get('fail-msg') }}</div>
    @endif

    <div class="input-field{{ $errors->has('date') ? ' has-error' : '' }}">
        @if ($errors->has('date'))
            <span class="help-block">
                <strong>{{ $errors->first('date') }}</strong>
            </span>
        @endif
        <input type="text" class="datepicker" name="date" id="date" placeholder="予約日を選択" value="{{ old('date') }}">
        <label for="date">来訪日</label>
    </div>

    <div class="input-field{{ $errors->has('time') ? ' has-error' : '' }}">
        @if ($errors->has('time'))
            <span class="help-block">
                <strong>{{ $errors->first('time') }}</strong>
            </span>
        @endif
        <input type="text" class="timepicker" name="time" id="time" placeholder="予約時間を選択"value="{{ old('time') }}">
        <label for="time">来訪時間</label>
    </div>

    <div class="input-field{{ $errors->has('people') ? ' has-error' : '' }} select-box">
        @if ($errors->has('people'))
            <span class="help-block">
                <strong>{{ $errors->first('people') }}</strong>
            </span>
        @endif
        <select name="people" id="people">
            <option value="" disabled selected>人数を選択</option>
            @for($i=1; $i<(int)$space->max_reserve_count; $i++)
                <option value="{{$i}}">{{$i}}人</option>
            @endfor
        </select>
        <label for="people">人数</label>
    </div>

    <div class="input-field{{ $errors->has('stay_hour') ? ' has-error' : '' }} select-box">
        @if ($errors->has('stay_hour'))
            <span class="help-block">
                <strong>{{ $errors->first('stay_hour') }}</strong>
            </span>
        @endif
        <select name="stay_hour" id="stay_hour">
            <option value="" disabled selected>滞在時間を選択</option>
            @for($i=1; $i<=3; $i++)
                <option value="{{$i}}">{{$i}} Hour</option>
            @endfor
        </select>
        <label for="stay_hour">滞在時間</label>
    </div>

    <div class="btn-box">
        <button class="btn waves-effect waves-light teal lighten-3" type="button" onclick="history.back()">
            戻る
        </button>
        <button class="btn waves-effect waves-light" type="submit">
            確認<i class="material-icons right">send</i>
        </button>

    </div>
</form>

@section('my-js')
    <script>
        //materialize select box styling
        $('select').material_select();

        //datepicker
        $(".datepicker").pickadate({
            monthsFull:  ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            monthsShort: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            weekdaysFull: ["日曜日", "月曜日", "火曜日", "水曜日", "木曜日", "金曜日", "土曜日"],
            weekdaysShort:  ["日", "月", "火", "水", "木", "金", "土"],
            weekdaysLetter: ["日", "月", "火", "水", "木", "金", "土"],
            labelMonthNext: "翌月",
            labelMonthPrev: "前月",
            labelMonthSelect: "月を選択",
            labelYearSelect: "年を選択",
            selectMonths: true,
            selectYears: 15,
            today: "今日",
            clear: "クリア",
            close: "閉じる",
            closeOnSelect: true,
            format: "yyyy-mm-dd",
            min: new Date()
        });

        //timepicker
        $('.timepicker').pickatime({
            default: 'now', // Set default time: 'now', '1:30AM', '16:30'
            fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
            twelvehour: false, // Use AM/PM or 24-hour format
            donetext: 'OK', // text for done-button
            cleartext: 'クリア', // text for clear-button
            canceltext: 'キャンセル', // Text for cancel-button
            autoclose: true, // automatic close timepicker
            ampmclickable: true, // make AM PM clickable
            aftershow: function(){} //Function for after opening timepicker
        });
    </script>
@endsection