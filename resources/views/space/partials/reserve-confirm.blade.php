<form class="reserve row" name="reserve" method="post" action="{{route('space.purchase')}}" id="reserve">
    @if (Session::has('fail-msg'))
        <div class="alert alert-error">{{ Session::get('fail-msg') }}</div>
    @endif

    <section class="input-check">
        <div class="">
            <label>来訪日</label>
            <p>{{ $params['date'] }}</p>
        </div>

        <div class="">
            <label>来訪時間</label>
            <p>{{ $params['time'] }}</p>
        </div>

        <div class="">
            <label>人数</label>
            <p>{{ $params['people'] }} 人</p>
        </div>

        <div class="">
            <label>滞在時間</label>
            <p>{{ $params['stay_hour'] }} 時間</p>
        </div>

        <div class="">
            <label>合計金額</label>
            <p>¥ {{ number_format($params['sum_price']) }}</p>
        </div>
    </section>

    <div class="btn-box">
        <button class="btn waves-effect waves-light teal lighten-3" type="button" onclick="history.back()">
            戻る
        </button>
        <button class="btn waves-effect waves-light" type="submit">
            予約<i class="material-icons right">credit_card</i>
        </button>
    </div>
    <div class="hidden">
        <input type="hidden" name="sid" value="{{$space->id}}">
        <script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="pk_test_qoJbsKeFVpBwqL3XrpUK5tez"
                data-amount="{{$params['sum_price']}}" 　
                data-name="{{Config::get('app.name')}}"
                data-description="スペース料金"
                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                data-locale="ja"
                data-currency="jpy">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
    </div>
</form>

@section('my-js')
    <script>
        //materialize select box styling
        $('select').material_select();

        //datepicker
        $(".datepicker").pickadate({
            monthsFull:  ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            monthsShort: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            weekdaysFull: ["日曜日", "月曜日", "火曜日", "水曜日", "木曜日", "金曜日", "土曜日"],
            weekdaysShort:  ["日", "月", "火", "水", "木", "金", "土"],
            weekdaysLetter: ["日", "月", "火", "水", "木", "金", "土"],
            labelMonthNext: "翌月",
            labelMonthPrev: "前月",
            labelMonthSelect: "月を選択",
            labelYearSelect: "年を選択",
            selectMonths: true,
            selectYears: 15,
            today: "今日",
            clear: "クリア",
            close: "閉じる",
            closeOnSelect: true,
            format: "yyyy-mm-dd",
            min: new Date()
        });

        //timepicker
        $('.timepicker').pickatime({
            default: 'now', // Set default time: 'now', '1:30AM', '16:30'
            fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
            twelvehour: false, // Use AM/PM or 24-hour format
            donetext: 'OK', // text for done-button
            cleartext: 'クリア', // text for clear-button
            canceltext: 'キャンセル', // Text for cancel-button
            autoclose: true, // automatic close timepicker
            ampmclickable: true, // make AM PM clickable
            aftershow: function(){} //Function for after opening timepicker
        });
    </script>
@endsection