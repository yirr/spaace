{{-- Require SpaceData($space) --}}
<form class="register col s12" name="reserve" method="post" action="{{route('space.store')}}" id="reserve">
    {{ csrf_field() }}

    {{--inputs--}}
    {{--スペース名--}}
    <h3>基本情報</h3>
    <section class="base-info row">
        <div class="row">
            <div class="input-field{{ $errors->has('name') ? ' has-error' : '' }} col s12">
                <i class="material-icons prefix">pin_drop</i>
                <input type="text" class="validate" name="name" id="name" value="{{ $params['name'] }}" disabled>
                <label for="name">スペース名</label>
            </div>
            @if ($errors->has('name'))
                <span class="help-block"> <strong>{{ $errors->first('name') }}</strong> </span>
            @endif
        </div>

        {{--一時間あたりの利用料金--}}
        <div class="row">
            <div class="input-field{{ $errors->has('price') ? ' has-error' : '' }} col s9">
                <i class="material-icons prefix">monetization_on</i>
                <input type="text" class="validate" name="price" id="price" value="{{ $params['price'] }}" disabled>
                <label for="price">一時間あたりの利用料金</label>
            </div>
            @if ($errors->has('price'))
                <span class="help-block"> <strong>{{ $errors->first('price') }}</strong> </span>
            @endif
        </div>

        {{--郵便番号--}}
        <div class="row s12">
            <div class="input-field{{ $errors->has('zip') ? ' has-error' : '' }} col s6">
                <i class="material-icons prefix">edit_location</i>
                <input type="text" class="validate" name="zip" id="zip" value="{{ $params['zip'] }}" disabled>
                <label for="zip">郵便番号</label>
            </div>
            @if ($errors->has('zip'))
                <span class="help-block"> <strong>{{ $errors->first('zip') }}</strong> </span>
            @endif
        </div>

        {{--住所--}}
        <div class="row">
            <div class="input-field{{ $errors->has('address') ? ' has-error' : '' }} col s12">
                <i class="material-icons prefix">edit_location</i>
                <input type="text" class="validate" name="address" id="address" value="{{ $params['address'] }}"
                       disabled>
                <label for="address">住所</label>
            </div>
            @if ($errors->has('address'))
                <span class="help-block"> <strong>{{ $errors->first('address') }}</strong> </span>
            @endif
        </div>

        {{--説明--}}
        <div class="row">
            <div class="input-field col s12">
                <i class="material-icons prefix">edit</i>
                <textarea id="description" class="materialize-textarea validate" name="description"
                          disabled>{{ $params['description'] }}</textarea>
                <label for="description">スペースの説明</label>
            </div>
            @if ($errors->has('description'))
                <span class="help-block"> <strong>{{ $errors->first('description') }}</strong> </span>
            @endif
        </div>

        {{--画像--}}
        <h3>画像</h3>
        <div class="row image">
            <img src="{{asset('storage/tmp/'.$params['image'])}}" alt="アップロード画像" style="max-height: 400px">
        </div>
    </section>

    {{--オプション--}}
    <h3>オプション</h3>
    <section class="options row">
        <h4>設備</h4>
        <div class="row">
            <div class="col s6 option">
                @if(!empty($params['opt']))
                    <ul>
                        @foreach($params['opt'] as $option)
                            <li>
                                <i class="material-icons prefix">check_box</i>{{$space_options->where('id', $option)->first()->name}}
                            </li>
                        @endforeach
                    </ul>
                @else
                    <p>なし</p>
                @endif
            </div>
        </div>

        <h4>飲食の提供</h4>
        <div class="row">
            @foreach($foods as $key => $food)
                <div class="col s4">
                    <input class="with-gap" name="foods" type="radio" id="foods{{$key}}" value="{{$key}}"
                           @if((int)$params['foods']===$key) checked @endif disabled>
                    <label for="foods{{$key}}">{{$food}}</label>
                </div>
            @endforeach
            @if ($errors->has('foods'))
                <span class="help-block"> <strong>{{ $errors->first('foods') }}</strong> </span>
            @endif
        </div>
    </section>


    {{--button--}}
    <div class="btn-box row s12">
        <button class="btn waves-effect waves-light teal lighten-3" type="button" onclick="history.back()">
            戻る
        </button>
        <button class="btn waves-effect waves-light" type="submit">
            確認<i class="material-icons right">send</i>
        </button>
    </div>
</form>