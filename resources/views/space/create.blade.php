@extends('layouts.master')

@section('title')
    スペースの登録
@endsection

@section('main-class')
    space create
@endsection

@section('main')
    <div class="row">
        <h2 class="">スペースの登録</h2>
    </div>
    {{--登録フォーム--}}
    @include('space/partials/create-form')
@endsection