@extends('layouts.master')

@section('title')
    スペース登録完了
@endsection

@section('main-class')
    space purchase
@endsection

@section('main')
    <p>スペースの登録が完了しました!</p>
    <div class="btn-box">
        <a class="waves-effect waves-light btn-large" href="{{route('space.show', ['space' => $space->id])}}">確認する</a>
    </div>
@endsection