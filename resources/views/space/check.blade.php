@extends('layouts.master')

@section('title')
    スペース登録内容確認
@endsection

@section('main-class')
    space check
@endsection

@section('main')
    <div class="row">
        <h2 class="">スペース登録内容確認</h2>
    </div>
    {{--登録フォーム--}}
    @include('space/partials/check-form')
@endsection

@section('my-js')
    <script>
        //ES6の即時関数
        // jQueryなければ{}だけでもよい
        $(()=>{
            $('#description').trigger('autoresize');
        });
    </script>
@endsection