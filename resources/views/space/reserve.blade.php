@extends('layouts.master')

@section('title')
    スペース予約
@endsection

@section('main-class')
    space reserve reserve-flow
@endsection

@section('main')
    <div class="space-info">
        <div class="image">
            <img src="{{asset(config('const.space.image_dir').$space->image)}}" alt="space image">
        </div>
        <div class="text">
            <h2 class="name">{{$space->name}}</h2>
            <span class="rate">
                @for($i=0; $i<5; $i++)
                    <i class="fa fa-star" aria-hidden="true"></i>
                @endfor
                (23)
            </span>
            <span class="price">¥ {{number_format($space->price)}}</span>
        </div>
    </div>
    {{--Form--}}
    @include('space/partials/reserve-confirm')
@endsection