@extends('layouts.master')

@section('title')
    スペース予約
@endsection

@section('main-class')
    space purchase
@endsection

@section('main')
    <p>予約が完了しました!</p>
    <div class="btn-box">
        <a class="waves-effect waves-light btn-large" href="{{route('home.index')}}">マイページで確認</a>
    </div>
@endsection