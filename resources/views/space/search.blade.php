@extends('layouts.master')

@section('title')
    スペース一覧
@endsection

@section('main-class')
    space search
@endsection

@section('main')
    <form action="{{route('space.search')}}" method="get" name="search" class="search">
        <div class="search-box input-field">
            <i class="material-icons prefix">find_replace</i>
            <input type="text" name="w" value="" placeholder="「名古屋」などエリアを入力">
        </div>
        <div class="search-around">
            <a class="waves-effect waves-light btn"><i class="material-icons left">location_searching</i>現在地から探す</a>
        </div>
    </form>
    @if(!empty($keyword))
        <p class="search-word">
            <i class="material-icons prefix">search</i> <b>{{$keyword}}</b> の検索結果
        </p>
    @endif
    <ul class="space-list">
        @foreach($spaces as $space)
            <li class="card">
                <a href="{{route('space.show',['id' => $space->id])}}" class="waves-effect">
                    <div class="image"
                         style="background: url({{asset(config('const.space.image_dir').$space->image)}}) center center /cover no-repeat">
                    </div>
                    <div class="text">
                        <b class="name">{{$space->name}}</b>
                        <span class="price">¥ {{number_format($space->price)}} / 1h</span>
                        <span class="rate">
                            @for($i=0; $i<5; $i++)
                                <i class="fa fa-star" aria-hidden="true"></i>
                            @endfor
                            (23)
                        </span>
                    </div>
                    <div class="place">
                        <i class="material-icons">location_on</i>
                        {{$space->address_s}}
                    </div>
                </a>
            </li>
        @endforeach
    </ul>
    {{$spaces->appends(['w' => $keyword])->links()}}
@endsection

@section('my-js')
    <script>
        const URL = {
            geo: "{{route('api.geo')}}"
        };
    </script>
    <script src="{{asset('js/map.js')}}"></script>
    <script src="{{asset('js/search.js')}}"></script>
@endsection
