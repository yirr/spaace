@extends('layouts.master')

@section('title')
    ログイン
@endsection

@section('main-class')
    login
@endsection

@section('main')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            <div class="input-field{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}"
                                       required autofocus>
                                <label for="email">メールアドレス</label>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="input-field{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" type="password" class="validate" name="password" required>
                                <label for="password">パスワード</label>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="remember">
                                <div class="checkbox">
                                    <input type="checkbox" class="filled-in" id="filled-in-box" checked="checked"
                                           name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label for="filled-in-box">ログインを記憶する</label>
                                </div>
                            </div>

                            <div class="btn-box">
                                <button type="submit" class="btn waves-effect waves-light">
                                    ログイン
                                </button>
                            </div>
                            <div class="col-md-8 col-md-offset-4">
                                <a class="remind" href="{{ route('password.request') }}">
                                    パスワードを忘れた？
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
