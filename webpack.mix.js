var mix = require('laravel-mix');

// version does not work in hmr mode
if (process.env.npm_lifecycle_event !== 'hot') {
    mix.version()
}

const path = require('path');
mix.webpackConfig({
    module: {
        rules: [{
            test: /\.scss/,
            enforce: "pre",
            loader: 'import-glob-loader'
        }]
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'public'),
    }
})
.js('resources/assets/js/app.js', 'public/js')
.sass('resources/assets/sass/app.scss', 'public/css');

//browserSync
mix.browserSync({
    files: [
        "resources/views/**/*.blade.php",
        "resources/assets/*.*",
        "public/**/*.*",

    ],
    proxy: {
        target: "localhost:8000",
    },
    open: true,
});

//Cache Busting
if (mix.inProduction()) {
    mix.version();
}